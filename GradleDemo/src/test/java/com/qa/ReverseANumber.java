package com.qa;


//  3- Write a function to reverse a number in Java?
public class ReverseANumber {

	public static void main(String[] args) {
		
		long num = 9873245;
		
		System.out.println("Before: "+num);
		
		System.out.println("After: "+reverseNumber(num));

	}
	
	public static long reverseNumber(long num) {
		
		long reverNum = 0;
		
		while(  num!=0 ) {
			reverNum = (reverNum*10) + num%10 ;			
			num = num / 10;
		}
		
		return reverNum;
	}

}
