package com.qa;

import java.util.HashMap;
import java.util.Map;

public class CountNumberOfWordsInString {

	public static void main(String[] args) {
		String someData = "This is program for test number of count in string, this is good program for initial test";
		
		String[] arr = someData.split(" ");
		
		HashMap<String, Integer> map = new HashMap<String, Integer>();		
		for(int i=0 ; i< arr.length; i++) {
			
			if(map.containsKey(arr[i])) {
				int count  = map.get(arr[i]);
				map.put(arr[i], count+1);
			}else {
				map.put(arr[i], 1);
			}			
		}
		System.out.println(map);
		
		System.out.println("Using for loop: ");
		for(Map.Entry entry: map.entrySet()) {
			System.out.println(entry.getKey() + " "+ entry.getValue());
		}
	}
}
		