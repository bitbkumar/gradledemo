package com.qa;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class NameWordSort {

	public static void main(String[] args) {
		
		String name = "Singh Kumar Birender";
		String[] arr = name.split(" ");
		List<String> list = new ArrayList<String>();				
		for(int i=0; i<arr.length; i++) {
			list.add(arr[i]);
			//System.out.println(arr[i]);
		}
		Collections.sort(list);
		Iterator<String> it = list.iterator();
		while(it.hasNext()){
			System.out.println(it.next());
		}
	}

}
