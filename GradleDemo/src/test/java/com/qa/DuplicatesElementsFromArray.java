package com.qa;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

// 1- Write code to filter duplicate elements from an array and print as a list?
public class DuplicatesElementsFromArray {

	public static void main(String[] args) {

		List<String> list = new ArrayList<String>();
		
		//Add elements 0-9
		for(int i=0 ; i<10 ; i++) {
			list.add(String.valueOf(i));
		}
		
		//add again elements 0-5
		for(int i=0 ; i<5 ; i++) {
			list.add(String.valueOf(i));
		}
		
		System.out.println("Before :"+list);		
		System.out.println("After :"+ findDuplicates(list));
		
		// Array containing duplicates elements
		String[] dupArr = {"Birender","Kumar", "Singh", "Chanchal", "Singh", "Abhivardhan", "Singh", "Birender", "Chanchal"};
		System.out.println("Duplicates from Array: "+ findDuplicatesFromArray(dupArr));
		
	}

	//find duplicates and store in the HashSet and return it.
	public static Set<String> findDuplicates(List<String> listOfEuplicatesElements) {
		
		Set<String> resultSet = new HashSet<>();
		Set<String> tempSet = new HashSet<>();
		
		//loop through all elements in list
		for(String str : listOfEuplicatesElements) {
			
			//if same elements get added again in set it return false, and not false means true, 
			//and condition passes and all duplicates get added to other set.
			if(!tempSet.add(str)) {
				resultSet.add(str);
			}
		}
		return resultSet;
	}
	
	public static  Set<String> findDuplicatesFromArray(String[] dupArr) {
		
		Set<String> resultSet = new HashSet<>();
		Set<String> tempSet = new HashSet<>();
		
		for(String str: dupArr) {
			if(!tempSet.add(str)) {
				resultSet.add(str);
			}
		}
		return resultSet;
	}
	
}
