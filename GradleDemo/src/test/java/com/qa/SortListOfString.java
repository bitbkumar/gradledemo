package com.qa;

import java.util.Arrays;

//2- Write code to sort the list of strings using Java collection?
public class SortListOfString {

	public static void main(String[] args) {
		String[] arr = {"Jan", "Feb" ,"March", "April", "may","June","July","Aug","Sep","oct","Nov", "Dec"};
		
		System.out.println("Before: ");
		showElements(arr);
		
		Arrays.sort(arr);
		
		System.out.println("\n");
		System.out.println("After: ");
		showElements(arr);
		
		System.out.println("\n");
		System.out.println("After case insensitive order: ");
		Arrays.sort(arr, String.CASE_INSENSITIVE_ORDER);
		showElements(arr);
		
	}
	
	public static void showElements(String[] arr) {
		for(String str: arr) {
			System.out.print(" "+str);
		}
	}

}
